﻿using System;
using System.Net;
using System.Net.Cache;
using System.Text;
using WebChainApi;
using System.Runtime;
using Newtonsoft.Json;

namespace esignsignature_sample
{
    public class Util
    {
        public static string ToHex(string url) =>
            BitConverter.ToString(Encoding.Default.GetBytes(url)).Replace("-", "");
        
        public static string HexToString(string hexString)
        {
            if (hexString == null)
                throw new ArgumentNullException($"{nameof(hexString)} must not be null");
            if (hexString.Length % 2 != 0)
                throw new ArgumentException($"{nameof(hexString)} must have an even length", nameof(hexString));
            var bytes = new byte[hexString.Length / 2];
            for (var i = 0; i < bytes.Length; i++)
            {
                var currentHex = hexString.Substring(i * 2, 2);
                bytes[i] = Convert.ToByte(currentHex, 16);
            }

            return Encoding.GetEncoding("ISO-8859-1").GetString(bytes);
        }

        public static TResponse WebChainPostRequest<TRequest, TResponse>(TRequest data, string endpoint = null)
        {
            var url = $"https://sandbox.edoclogic.com/ISAPISB/RIP3.dll/REST{(endpoint != null ? $"/{endpoint}" : "")}";
            const string defaultContent = "application/json";
            var request = WebChain.NewRequest(url)
                .Method(WebRequestMethods.Http.Post)
                .Config(req =>
                {
                    req.Accept = defaultContent;
                    req.ContentType = defaultContent;
                });

            var requestData = JsonConvert.SerializeObject(data, new JsonSerializerSettings()
            {
                Formatting = Formatting.None,
                NullValueHandling = NullValueHandling.Ignore
            });

            request.Body(requestData);
            var response = request.ReadResponse();
            return JsonConvert.DeserializeObject<TResponse>(response);
        }
        
        public static String GetDocument<TRequest>(TRequest data, string endpoint = null)
        {
            var url = $"https://sandbox.edoclogic.com/ISAPISB/RIP3.dll/REST{(endpoint != null ? $"/{endpoint}" : "")}";
            const string defaultContent = "application/json";
            var request = WebChain.NewRequest(url)
                .Method(WebRequestMethods.Http.Post)
                .Config(req =>
                {
                    req.Accept = defaultContent;
                    req.ContentType = defaultContent;
                });

            var requestData = JsonConvert.SerializeObject(data, new JsonSerializerSettings()
            {
                Formatting = Formatting.None,
                NullValueHandling = NullValueHandling.Ignore
            });

            request.Body(requestData);
            return request.ReadResponse();
        }
    }
}
