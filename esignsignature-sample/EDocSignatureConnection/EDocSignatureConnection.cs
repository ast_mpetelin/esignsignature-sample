﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace esignsignature_sample
{
    public partial class EDocSignatureConnection
    {
        private string _session = null;

        string Session
        {
            get
            {
                if (_session != null)
                    return _session;

                var connectResult = Util.WebChainPostRequest<SessionsRequest, SessionsResponse>(new SessionsRequest
                {
                    LoginType = "User",
                    ControlId = "EDS212",
                    Username = "brittany@EDS212",
                    Password = "AccessSoftek21!",
                }, "SESSIONS");

                return connectResult.Result ? connectResult.Session : null;
            }
        }

        public GetStatusResponse GetStatus(string packageId) =>
            Util.WebChainPostRequest<PostRequest, GetStatusResponse>(new PostRequest()
        {
            Action = "GETPACKAGESIGNINGSTATUS",
            Session = Session,
            PackageId = packageId
        }, "PACKAGES");

        public CreatePackageResponse CreatePackage(string name) =>
            Util.WebChainPostRequest<PostRequest, CreatePackageResponse>(new PostRequest()
            {
                Action = "CREATESIGNING",
                Session = Session,
                PackageName = name,
                User = "Max",
                SendNow = true,
                NotificationType = "Public",
                NotificationName = "Petelin Maksim",
                NotificationEmail = "mpetelin@accesssoftek.com",
                RedirectUrl = "SQL.ru",
                Templates = GetTemplates(),
                Signers = GetSigners()
            }, "TEMPLATES");
        
        //Download document
        public string GetDfc(string documentId) =>
            Util.GetDocument<PostRequest>(new PostRequest()
            {
                ControlId = "EDS212",
                Session = Session,
                Action = "GETDFC",
                DocumentId = documentId,
                OutputType = "PDF"
            }, "DFC");

        List<PackageTemplate> GetTemplates() =>
            new List<PackageTemplate>()
            {
                new PackageTemplate()
                {
                    TemplateName = "signing document 1",
                    IndexFields = new object[]
                    {
                        new {name = "FirstName", value = "John"},
                        new {name = "LastName", value = "Daw"},
                        new {name = "Age", value = "37"},
                    }
                } /*,
                new PackageTemplate()
                {
                    TemplateName = "reference document 1",
                    IndexFields = new object[]
                    {
                        new {name = "FirstName", value = "Maria"},
                        new {name = "LastName", value = "Curie"},
                        new {name = "Age", value = "41"},
                    }
                },
                new PackageTemplate()
                {
                    TemplateName = "requestedDocument1"
                }*/
            };

        List<Signer> GetSigners() =>
            new List<Signer>()
            {
                new Signer()
                {
                    Name = "John Dow",
                    Email = "mpetelin@accesssoftek.com",
                    Role = "Applicant"
                },
                new Signer()
                {
                    Name = "Maria Curie",
                    Email = "petelinmn@gmail.com",
                    Role = "CoApplicant"
                }
            };
    }
}
