﻿using Newtonsoft.Json;

namespace esignsignature_sample
{
    class SessionsRequest
    {
        [JsonProperty("logintype")]
        public string LoginType { get; set; }
        [JsonProperty("controlid")]
        public string ControlId { get; set; }
        [JsonProperty("username")]
        public string Username { get; set; }
        [JsonProperty("password")]
        public string Password { get; set; }
        [JsonProperty("passwordhash")]
        public string PasswordHash { get; set; }
    }
}