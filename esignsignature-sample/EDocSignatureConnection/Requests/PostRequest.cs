﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace esignsignature_sample
{
    public class PostRequest
    {
        [JsonProperty("action")]
        public string Action { get; set; }
        [JsonProperty("controlid")] 
        public string ControlId { get; set; } = "EDS212";
        [JsonProperty("host")]
        public string Host { get; set; } = "127.0.0.1";
        [JsonProperty("session")]
        public string Session { get; set; }
        [JsonProperty("outputtype")]
        public string OutputType { get; set; }
        [JsonProperty("includeaudit")]
        public int IncludeAudit { get; set; } = 1;
        [JsonProperty("docid")]
        public string DocumentId { get; set; }
        [JsonProperty("pkgname")]
        public string PackageName { get; set; }
        [JsonProperty("user")]
        public string User { get; set; }
        [JsonProperty("sendnow")]
        public bool SendNow { get; set; }
        [JsonProperty("notificationtype")] 
        public string NotificationType { get; set; }
        [JsonProperty("templates")]
        public List<PackageTemplate> Templates { get; set; }
        [JsonProperty("signers")]
        public List<Signer> Signers { get; set; }
        [JsonProperty("redirecturl")] 
        public string RedirectUrl { get; set; }
        [JsonProperty("pkgid")] 
        public string PackageId { get; set; }

        [JsonProperty("notificationname")] 
        public string NotificationName { get; set; }
        [JsonProperty("notificationemail")] 
        public string NotificationEmail { get; set; }

    }
    
    public class PackageTemplate
    {
        [JsonProperty("templatename")]
        public string TemplateName { get; set; }
        [JsonProperty("indexfields")]
        public object IndexFields { get; set; }
    }
    
    public class Signer
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("role")]
        public string Role { get; set; }
    }
}
