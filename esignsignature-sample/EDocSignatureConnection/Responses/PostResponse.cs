﻿using Newtonsoft.Json;

namespace esignsignature_sample
{
    public class PostResponse
    {
        [JsonProperty("result")]
        public string Result { get; set; }
        [JsonProperty("error")]
        public string Error { get; set; }
        [JsonProperty("systemerror")]
        public string SystemError { get; set; }
    }
}