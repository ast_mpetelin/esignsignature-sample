﻿using System;
using Newtonsoft.Json;

namespace esignsignature_sample
{
    public class GetStatusResponse : PostResponse
    {
        [JsonProperty("signingstatus")]
        public PackageInfo[] SigningStatus { get; set; } 
    }

    public class PackageInfo
    {
        [JsonProperty("pkgid")]
        public string PackageId { get; set; }
        [JsonProperty("pkgstatus")]
        public string PackageStatus { get; set; }
        [JsonProperty("pkgmodifiedon")]
        public DateTime PackageModifiedOn { get; set; }
        [JsonProperty("signername")]
        public string SignerName { get; set; }
        [JsonProperty("signeremail")]
        public string SignerEmail { get; set; }
    }
}