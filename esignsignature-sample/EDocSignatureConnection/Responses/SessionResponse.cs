﻿using Newtonsoft.Json;

namespace esignsignature_sample
{
    class SessionsResponse
    {
        [JsonProperty("result")]
        public bool Result { get; set; }
        [JsonProperty("session")]
        public string Session { get; set; }
        [JsonProperty("permissions")]
        public string[] Permissions { get; set; }
    }
}