﻿using Newtonsoft.Json;

namespace esignsignature_sample
{
    public class CreatePackageResponse : PostResponse
    {
        [JsonProperty("packageid")]
        public string PackageId { get; set; }
        [JsonProperty("handoffs")]
        public Handoff[] Handoffs { get; set; }
        [JsonProperty("documents")]
        public Document[] Documents { get; set; }
        
        public class Handoff
        {
            [JsonProperty("handoff")]
            public string Value { get; set; }
            [JsonProperty("name")]
            public string Name { get; set; }
            [JsonProperty("role")]
            public string Role { get; set; }
            [JsonProperty("email")]
            public string Email { get; set; }
        }
        
        public class Document
        {
            [JsonProperty("docid")]
            public string DocumentId { get; set; }
        }
    }
}