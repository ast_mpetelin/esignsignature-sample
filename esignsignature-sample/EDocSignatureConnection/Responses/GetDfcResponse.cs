﻿using Newtonsoft.Json;

namespace esignsignature_sample
{
    public class GetDfcResponse : PostResponse
    {
        [JsonProperty("file")]
        public string File { get; set; }
    }
}