﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using esignsignature_sample;

namespace esignsignature_sample
{
    static class Program
    {
        static void Main(string[] args)
        {
            RunStandardFlow();
        }

        static void RunStandardFlow()
        {
            var eDocConnection = new EDocSignatureConnection();

            var packageName = $"MyPackage ({DateTime.Now.ToString("dd.MM.yyyy HH:mm")}";
            var pkg = eDocConnection.CreatePackage(packageName);
            var signingLink = pkg.Handoffs.First();
            var docId = pkg.Documents.First().DocumentId;
            Console.WriteLine($"Created new package \"{packageName}\" with Id: {pkg.PackageId}");
            Console.WriteLine($"Document Id: {docId}");
            Console.WriteLine($"Please follow the link for signing document:");
            Console.WriteLine(signingLink.Value);

            for (;;)
            {
                Console.Write(".");
                var status = eDocConnection.GetStatus(pkg.PackageId);
                var statusText = status.SigningStatus?.FirstOrDefault()?.PackageStatus;

                if (statusText != "Completed")
                {
                    Thread.Sleep(3000);
                    continue;
                }

                Console.WriteLine("done");
                break;
            }
            
            var doc = eDocConnection.GetDfc(docId);
            Console.WriteLine(doc);
        }
    }
}
